### A Pluto.jl notebook ###
# v0.19.26

using Markdown
using InteractiveUtils

# ╔═╡ 7a997d8e-fd5c-11ed-1ebb-3521e86439d7
begin
	import Pkg
	Pkg.activate("..")
	using FiniteFunctions
end

# ╔═╡ 26c727f5-4793-425f-833d-3240e92de460
begin
	using RDatasets
	using Plots
end

# ╔═╡ cd0b9c56-fb1c-4f03-a455-1da9c68c2a7a
const FF = FiniteFunctions

# ╔═╡ 68bb9c08-2a58-4da0-bf04-8616ff4c4a60
iris = dataset("datasets", "iris")

# ╔═╡ c03ec0fa-5054-427b-8abd-fdd0c1b79457
flower_to_species = FF.table_to_ff(
	iris, x -> RDatasets.rownumber(x), x -> x["Species"]
)

# ╔═╡ 028100f0-db13-4ac5-984c-349615bf53b6
FF.preimage(flower_to_species)

# ╔═╡ 23cdca3b-ca33-4867-9b40-9a8ab9df471f
FF.rarify(FF.preimage(flower_to_species))

# ╔═╡ 7302df0f-e20c-485c-bf19-06081fae68f5
length ∘ collect ∘ FF.preimage(flower_to_species)

# ╔═╡ d2dd3c1d-4cd0-478a-914e-b76205e6206d
FF.rarify(length ∘ collect ∘ FF.preimage(flower_to_species))

# ╔═╡ bb968d09-f547-409d-85fa-46cb20382418
FF.preimage(flower_to_species)("setosa")

# ╔═╡ 418a6540-2968-44ab-ae72-f8b4da665f25
flower_to_sepal = FF.table_to_ff(
	iris, x -> RDatasets.rownumber(x), x -> [x["SepalLength"], x["SepalWidth"]]
)

# ╔═╡ a4f54c6b-17e9-4adf-9291-a9c556c0bc90
function plot(ff::FF.FiniteFunction)
	x = sort(collect(ff.src))
	Plots.plot(x, ff.(x))
end

# ╔═╡ 8519bf38-76e8-4f8b-8b82-6178464b4642
function hist(ff::FF.FiniteFunction)
	x = sort(collect(ff.src))
	Plots.histogram(x, ff.(x))
end

# ╔═╡ fa2449c3-b951-4d29-965d-a1e54ed32092
FF.rarify(FF.mono(FF.preimage(flower_to_species)("setosa")))

# ╔═╡ 87c383ba-4fd9-46a9-b0eb-655141e65422
flower_to_sepal ∘ FF.mono(FF.preimage(flower_to_species)("setosa"))

# ╔═╡ 4fbb232e-2930-4bd1-8ec4-370a7d28e653
plot(first ∘ flower_to_sepal ∘ FF.mono(FF.preimage(flower_to_species)("setosa")))

# ╔═╡ 3e104299-05b2-43a6-96a2-601065db64e1
hist(first ∘ flower_to_sepal ∘ FF.mono(FF.preimage(flower_to_species)("setosa")))

# ╔═╡ c62342bb-1729-4b4e-93be-cc1a3897e854
baseball = RDatasets.dataset("plyr", "baseball")

# ╔═╡ 1c8cbad1-aa9a-4e1a-bb38-6d54bc7b8a7f
rowid_to_numid = FF.table_to_ff(baseball,
	x -> RDatasets.rownumber(x),
	x -> x["NumID"],
)

# ╔═╡ 9c0e71b9-ef23-4c75-a0f6-7d0303dd5f5c
FF.invertible(rowid_to_numid)

# ╔═╡ 060e520a-f689-4595-a220-ed86003cec74
numid_to_rowid = FF.inverse(rowid_to_numid)

# ╔═╡ 13b2664d-88ee-4dbc-b719-7427786d05a3
numid_to_id = FF.table_to_ff(baseball,
	x -> x["NumID"],
	x -> String(x["ID"])
)

# ╔═╡ 76b2ac3c-b6ee-49c0-be7b-49bcfcf41b79
FF.invertible(numid_to_id)

# ╔═╡ 49be5273-47ad-40d8-a3c3-26733a275fac
hist(length ∘ collect ∘ FF.preimage(numid_to_id))

# ╔═╡ 5df1b2d7-c73e-43c3-9eb2-306b82532710
FF.preimage(numid_to_id)("youngcy01")

# ╔═╡ b2dd7041-66c6-42d8-ba7c-09f17e9e60d6
sort(numid_to_id.tgt)

# ╔═╡ f8676e40-00ab-40e5-a0ac-e1c9259f2d0e
rowid_to_team = FF.table_to_ff(baseball,
	x -> RDatasets.rownumber(x),
	x -> x["Team"],
)

# ╔═╡ 31e17a8e-9b8d-49ec-ba82-38a427042acd
id_to_team = FF.rarify(rowid_to_team ∘ numid_to_rowid ∘ FF.mono(FF.preimage(numid_to_id)("youngcy01")))

# ╔═╡ def617c5-9a6d-44c1-adbd-a35c995cb048
FF.invertible(id_to_team)

# ╔═╡ 6df2cd65-dc32-4402-aac3-c607ac2fb12e
FF.rarify(collect ∘ FF.preimage(id_to_team))

# ╔═╡ 7fb50ef7-e84d-4e67-9cdf-f16d74315c57
FF.rarify(rowid_to_team ∘ numid_to_rowid ∘ FF.mono(FF.preimage(numid_to_id)("youngcy01")))

# ╔═╡ Cell order:
# ╠═7a997d8e-fd5c-11ed-1ebb-3521e86439d7
# ╠═26c727f5-4793-425f-833d-3240e92de460
# ╠═cd0b9c56-fb1c-4f03-a455-1da9c68c2a7a
# ╠═68bb9c08-2a58-4da0-bf04-8616ff4c4a60
# ╠═c03ec0fa-5054-427b-8abd-fdd0c1b79457
# ╠═028100f0-db13-4ac5-984c-349615bf53b6
# ╠═23cdca3b-ca33-4867-9b40-9a8ab9df471f
# ╠═7302df0f-e20c-485c-bf19-06081fae68f5
# ╠═d2dd3c1d-4cd0-478a-914e-b76205e6206d
# ╠═bb968d09-f547-409d-85fa-46cb20382418
# ╠═418a6540-2968-44ab-ae72-f8b4da665f25
# ╠═a4f54c6b-17e9-4adf-9291-a9c556c0bc90
# ╠═8519bf38-76e8-4f8b-8b82-6178464b4642
# ╠═fa2449c3-b951-4d29-965d-a1e54ed32092
# ╠═87c383ba-4fd9-46a9-b0eb-655141e65422
# ╠═4fbb232e-2930-4bd1-8ec4-370a7d28e653
# ╠═3e104299-05b2-43a6-96a2-601065db64e1
# ╠═c62342bb-1729-4b4e-93be-cc1a3897e854
# ╠═1c8cbad1-aa9a-4e1a-bb38-6d54bc7b8a7f
# ╠═9c0e71b9-ef23-4c75-a0f6-7d0303dd5f5c
# ╠═060e520a-f689-4595-a220-ed86003cec74
# ╠═13b2664d-88ee-4dbc-b719-7427786d05a3
# ╠═76b2ac3c-b6ee-49c0-be7b-49bcfcf41b79
# ╠═49be5273-47ad-40d8-a3c3-26733a275fac
# ╠═5df1b2d7-c73e-43c3-9eb2-306b82532710
# ╠═b2dd7041-66c6-42d8-ba7c-09f17e9e60d6
# ╠═f8676e40-00ab-40e5-a0ac-e1c9259f2d0e
# ╠═31e17a8e-9b8d-49ec-ba82-38a427042acd
# ╠═def617c5-9a6d-44c1-adbd-a35c995cb048
# ╠═6df2cd65-dc32-4402-aac3-c607ac2fb12e
# ╠═7fb50ef7-e84d-4e67-9cdf-f16d74315c57
