# Catlab.jl Integration

As a categorical library to analyze database schemas, write algorithms over them, and do categorical algebra, it seems to work well. It can provide great macro time support for categorical datatypes in julia functions. And these macros are actually like readable and embedded. It's cool

But it's not an ergonomic way to access or work with data. I think that we can probably change things and still get use out of all those cool algorithms and macros (and do really cool serialization and deserialization with Catlab.jl), but just using it to plot something is a lot of boilerplate and some problems are just completely unclear.
Big one is that in Catlab.jl ACSets, the user cannot access primary keys. Sorry to tell you bud but 95% of my job is finding, making, and juggling primary keys.
You could probably hack it in (i.e. ACSets have enough mathematical power), but it's annoying to make those schema.

In some ways, the difference is like the difference between static
and dynamic typing. Catlab.jl seems to focus on static typing by providing
a constant schema in the form of an Attributed C-Set instance.
Instead, I would like the dynamic version of that. I would like
to take a collection of mappings and then make that into an attributed c-set
instance.

Here's some ways it could work:

Input:

1. Set of finite functions: $F = (f_i, S_i, T_i)_i$.

Ideas:

1. Make a schema and use constructor to input $f_i, S_i, T_i$ along with all the objects.
2. Take a collection of finite functions and try and detect equality of keys and targets in $S_i$ and $T_i$. $\mathcal{O}(n^2)$ complexity naively, but
probably $\mathcal{O}(n)$ with hashing. (There is also the function `objectid` which gets a hash from the object id).
3. Treat everything like schema migration:
- $(f_i, S_i, T_i)$ is just the interval category $S_i \to T_i$.
- Composing two morphisms concatenates categories:
  $f_1 : X \to Y$, $f_2 : Y \to Z$ gives new category $C(f + g) = X \to Y \to Z$ with objects $X, Y, Z$ and arrows $f$ and $g$.
- We can evaluate and create a new schema.
- So whenever we create a new `FiniteFunction` using an existing finite `src` or finite `target`, it implicitly adds it to the same.
- We need a place to put PreimageCaches
- It must be easy to convert an attribute $X \to Y$ where $Y$ is a Julia
type into $X \to Y'$ where $Y'$ is a finite set.


I want things to look a lot like DataFrames.jl in many ways:
- Little recompilation
- It should be easy to add a new column.
- Catlab.jl puts a _lot_ in the types: object, names, column names, column types, etc. Some of this is in a Schema type parameter, and some others.
  (At least in the usual StructACSet instead of DynamicACSet).
- ACSetInterface.jl isn't great.

```
FiniteFunction <: Mapping{S, T} <: AbstractDict{S, T}
# But FiniteFunction includes _target_ information usually contained
# in the schema.

f_1 = FiniteFunction(..., object1, Float64)  # map (probably attribute)
f_2 = FiniteFunction(..., object1, object3)  # Aggregation
f_3 = FiniteFunction(..., object4, object1)  # Injective subset
f_4 = reduce_cond(+, f_1 \circ f_3, f_2 \circ f_3) # Aggregation over subset
# f_4 : object3 \to Float64

acset = DynamicACSet(
    f_1, f_2, f_3, f_4
)

serialize(file1, acset)
serialize(file2, schema(acset))

schema = deserialize(file2)
acset = deserialize(schema, file1)
```

Merge timeseries
```
[ date1,  ..., date3, ..., date2, ..., date4 ]
t1 = date1:period1:date2
t2 = date3:period1:date4
f_1 = FiniteFunction(..., t1, Float64)
f_2 = FiniteFunction(..., t2, Float64)

l = limit(include(t1, date1:period1:date4), include(t2, date1:period1:date4))
l2 = limit_to_colimit(l)
alternate(l2, f_1, f_2) creates morphism rom l2.obj to Float64
```

commutative diagram
l.obj -> t1 -> d
l.obj -> t2 -> d
t1 -> Float64
t2 -> Float64

We want d -> Float64

Idea: we want to make d the colimit of l.obj -> t1 and l.obj -> t2.

This works b/c in a topos, pullbacks are push outs when l.obj -> t1 and
t2 -> d are monomorphisms. (Like using the suboject classifier). Since
pull backs of monomorphisms are monomorphisms, we just need t1 -> d
or t2 -> d to be a monomorphism.

Once we have a colimit, then we can immediately get d -> Float64 using
the commutative square of l.obj -> t1 -> Float64 and l.obj -> t2 -> Float64

Maybe we can do something like this:

```
s = make_sheaf(t1, f1, t2, f2)

function make_sheaf(t1, f1, t2, f2)
    L = make_site(t1, t2)
    # L is a site for a sheaf.
    return glue(L, f1, f2)
end

s(top(s.L)) = FiniteFunction(f1 \cup f2, t1, t2)
```

j(1)=1
jj(x)=j(x)
j(x \cap y)=j(x)\capj(y)

Take a set $X$ say of times, let $\Omega$ be the set of subsets.
Then $j(X) = X$.

I'd like to get a time series of "max so far".
Suppose $T$ is the sheaf of floats over $X$, and let $t : 1 \to T$
be a particular element of that sheaf.

Then something like "find the max so far" is going to be a particular
operation $T \to T$.

We would like to construct this operation using logic:

We should automatically have things like $(X \to \R) \to (X \to \{0, 1\})$
given by $f \circ t$ where $f : \R \to \{0, 1\}$.

We would now like something like $before : x \mapsto \{y \leq x\}$.

Then combining $before$ and $t$ should give us $x \mapsto \{t(y) | y \leq x\}$.
Then we combine with an aggregation $g : 2^\R \to \R$:

$g \circ image(t) \circ before$.

The bigger the "windowing" operation, the fewer aggregation operations you can do.

"Good" operations can be composed easily:

Given it applied to [0, t] and [t, 1], you can easily get [0, 1]

Given $t_1(x)$ compute $t_2(x) = \max_{y \leq x} 



## Attempt 2

```
[ date1,  ..., date3, ..., date2, ..., date4 ]
t1 = date1:period1:date2
t2 = date3:period1:date4
f_1 = FiniteFunction(..., t1, Float64)
f_2 = FiniteFunction(..., t2, Float64)

L = make_site(t1, t2)
# Produces a category objects \emptyset, t1, t2, t1 \cup t2 with
# appropriate coverages (Grothendieck topology)

f = make_sheaf(L, f1, f2)
# Glues together f
f(top(L)) gives a finite function

# Aggregation + group by on a sheaf

reduce(op, f, group_by)
# But here group_by only works when it fits in to the sheaf. It specifies
# something like (t' \leq t)  or (t - 1 \leq t' \leq t+1)

Op can be a monoid, or it can be anything that let's you
1. Compute for base elements in the sheaf
2. Glue together given sets and their intersections + values on it.

L2 = make_full_site(date1:period1:date4)

```


## Unit System

```
@theory UnitSystem{Ob, Hom, IsoObject} <: ThCategory{Ob, Hom} begin
     IsoObject(A::Ob)::TYPE
     IsoHom(x::(ISOObject(A)), y::(IsoObject(A)))::Hom(A, A) \dashv A::Ob
end
```


