function rarify(f::FiniteFunction, reset_tgt=true)
    d = Dict([x => f(x) for x in f.src])
    FiniteFunction(FunctionDict(d), f.src, f.tgt)
end

function table_to_ff(table, src_f, tgt_f)
    d = Dict([src_f(r) => tgt_f(r) for r in Tables.rows(table)])
    table_len = length(Tables.rows(table))
    @assert length(keys(d)) == table_len begin
        "Length of dictionary $(length(keys(d))) does not match length of dataframe $table_len"
    end
    FiniteFunction(d)
end

function checkpoint(path, f, args...)
    # Path should probably depend on args...
    # Or maybe args -> path should be stored in
    # a kv store and we use the kv-store.
    # Unfortunately, kv-stores are a nightmare on eagle.
    if isfile(path)
        try
            return open(path, "r") do io
                Serialization.deserialize(io)
            end
        catch e
            @warn "Could not deserialize $path: $e"
        end
    end

    v = f(args...)
    open(path, "w") do io
        Serialization.serialize(io, v)
    end
    return v
end

function serialize(::Type{Dict}, ff::FiniteFunction)
    # TODO: Shouldn't this be recursive?
    return Dict([
        "mapping" => Dict((i => ff(i) for i in ff.src)),
        "tgt" => if ff.tgt isa DataType
            string(ff.tgt)
        else
            collect(ff.tgt)
        end
    ])
end

function deserialize(::Type{FiniteFunction}, d::Dict)
    # TODO: Shouldn't this be recursive?
    mapping = d["mapping"]
    if d["tgt"] isa String
        return FiniteFunction(
            mapping,
            eval(Symbol(d["tgt"]))
        )
    else
        return FiniteFunction(
            mapping,
            d["tgt"]
        )
    end
end

function threadrarify(ff::FiniteFunction)
    dict = Dict()
    p = Progress(length(ff.src))
    l = Threads.ReentrantLock()
    Threads.@threads for ix in collect(ff.src)
        @info "Processing $ix"
        y = ff(ix)
        next!(p)
        Threads.lock(l)
        try
            dict[ix] = y
        finally
            Threads.unlock(l)
        end
    end
    finish!(p)
    return FiniteFunction(dict)
end
