struct FiniteFunction{U, S, T}
    f::U
    src::S
    tgt::T
end

struct FunctionDict{S, T}
    f::Dict{S, T}
end

(f::FunctionDict{S, T})(x::S) where {S, T} = f.f[x]

struct FunctionVector{T}
    f::Vector{T}
end

(f::FunctionVector)(x) = f.f[x]

function FiniteFunction(f::Dict{S, T}) where {S, T}
    FiniteFunction(FunctionDict(f), keys(f), unique(collect(values(f))))
end

function FiniteFunction(f::Dict{S, T}, tgt) where {S, T}
    FiniteFunction(FunctionDict(f), keys(f), tgt)
end

function FiniteFunction(f::Vector{Pair{U, V}}) where {U, V}
    src = first.(f)
    tgt = last.(f)
    d = Dict(f)
    FiniteFunction(FunctionDict(d), src, tgt)
end

function FiniteFunction(f::Vector{Pair})
    src = first.(f)
    tgt = last.(f)
    d = Dict(f)
    FiniteFunction(FunctionDict(d), src, tgt)
end

function FiniteFunction(f::Vector{T}, tgt) where {T}
    FiniteFunction(FunctionVector(f), 1:length(f), tgt)
end

function FiniteFunction(f::Function, src)
    FiniteFunction(f, src, Any)
end

function (f::FiniteFunction)(x)
    f.f(x)
end

function Base.:∘(f::FiniteFunction, g::FiniteFunction)
    FiniteFunction(f.f ∘ g.f, g.src, f.tgt)
end

function Base.:∘(f::Function, g::FiniteFunction)
    FiniteFunction(f ∘ g.f, g.src, Any)
end

function reset_target(f::FiniteFunction, tgt)
    FiniteFunction(f.f, f.src, tgt)
end

function reset_target(f::FiniteFunction)
    FiniteFunction(f.f, f.src, unique(f.(f.src)))
end

function reset_target(f::FiniteFunction{FunctionDict})
    FiniteFunction(f.f, f.src, f.f.f)
end

function reset_target(f::FiniteFunction{FunctionVector})
    FiniteFunction(f.f, f.src, f.f.f)
end

function inverse(f::FiniteFunction)
    d = Dict([f(s) => s for s in f.src])
    return FiniteFunction(FunctionDict(d), f.tgt, f.src)
end

function image(f::FiniteFunction)
    return f.(f.src)
end

function reset_src(f::FiniteFunction, src)
    # Could also compose with (identity, src, f.src)
    FiniteFunction(f.f, src, f.tgt)
end

function invertible(ff::FiniteFunction)
    im = image(ff)
    length(ff.src) == length(ff.tgt) && allunique(im)
end

function injective(ff::FiniteFunction)
    allunique(image(ff))
end

function merge(f1::FiniteFunction, f2::FiniteFunction)
    src = vcat(f1.src, f2.src)
    @assert length(unique(src)) == length(src) "Cannot merge finite functions"
    srcmap = Dict(zip(src, vcat(ones(size(f1.src)), 2*ones(size(f2.src)))))
    FiniteFunction(src, Any) do index
        if srcmap[index] == 1
            f1(index)
        elseif srcmap[index] == 2
            f2(index)
        end
    end
end

function coproduct(f1::FiniteFunction, f2::FiniteFunction)
    src = vcat(f1.src, f2.src)
    srcmap = Dict(zip(src, vcat(ones(size(f1.src)), 2*ones(size(f2.src)))))
    FiniteFunction(src, Any) do index
        if srcmap[index] == 1
            f1(index)
        elseif srcmap[index] == 2
            f2(index)
        end
    end
end

function curry(f::FiniteFunction)
    section = Dict()
    for x in f.src
        x1, x2 = x # TODO: extend to other pattern matching?
        if !haskey(section, x1)
            section[x1] = []
        end
        push!(section[x1], x2)
    end
    x_to_y_to_xy = FiniteFunction(
        x -> FiniteFunction(y -> (x, y), section[x], f.src),
        keys(section), FiniteFunction
    )

    return f ∘ x_to_y_to_xy
end

function uncurry(f::FiniteFunction)
    new_src = []
    for k1 in f.src
        sub_f = f(k1)
        for k2 in sub_f.src
            push!(new_src, (k1, k2))
        end
    end
    FiniteFunction(
        (k1, k2) -> f(k1)(k2),
        new_src,
        Any  # Can we narrow this?
    )
end

"Reverse application"
rev_apply(x) = f -> f(x)

function zip(fs)
    src = first(fs).src
    @assert all(f.src == src for f in fs)
    FiniteFunction(
        x -> rev_apply(x).(fs),
        src,
        Tuple  # TODO: should be cartesian product of fs...
    )
end

function Base.:&(f1::FiniteFunction, f2::FiniteFunction)
    all ∘ zip((f1, f2))
end

function Base.:|(f1::FiniteFunction, f2::FiniteFunction)
    any ∘ zip((f1, f2))
end

function Base.:+(fs::FiniteFunction...)
    sum ∘ zip(fs)
end

function Base.:*(fs::FiniteFunction...)
    prod ∘ zip(fs)
end

function Base.:-(f::FiniteFunction)
    (x -> -x) ∘ f
end

function Base.:-(f1::FiniteFunction, f2::FiniteFunction)
    (x -> x[1]-x[2]) ∘ zip((f1, f2))
end

function Base.:/(f1::FiniteFunction, f2::FiniteFunction)
    (x -> x[1]/x[2]) ∘ zip((f1, f2))
end


function Base.:∘(transducer::Transducer, f::FiniteFunction)
    # TODO: Double check that this is even necessary...
    FiniteFunction(
        x -> transducer(f.f(x)),
        f.src,
        Any
    )
end

# f1 : B → 2^C and f2 : A → 2^B
#MapCat(f1) ∘ f2

"Filter f2 to keys that satisfy f1"
function filter(f1, f2::FiniteFunction)
    f2 ∘ mono(from_tag(f1 ∘ f2))
end

function Base.:(==)(f1::FiniteFunction, c)
    reset_target((x -> x == c) ∘ f1, [false, true])
end

function Base.:(==)(f1::FiniteFunction, f2::FiniteFunction)
    reset_target((x -> x[1] == x[2]) ∘ zip(f1, f2), [false, true])
end

function id(v)
    FiniteFunction(identity, v, v)
end
