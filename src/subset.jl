# Idea: Represent subsets as pullback squares
#
#    A   ->  \{true\}
#    |          |
#    \/         \/
#    B   -> \{false, true\}


# Desired Operations
#
# S is a subset of X

# S_1 \cap S_2 of subsets of X: S_1 \cap S_2 \subset S_1, S_1 \cap S_2 \subset S_2
# S_1 \cup S_2 of subsets of X: S_1 \subset S_1 \cup S_2, S_2 \subset S_1 \cup S_2
# Apply function X \to Y to S, including when X \to Y is a subset
#     f.(S)
# Pullback function Y \to X to S
#     from_tag(tag(S) ∘ f)
# Apply preimage X \to 2^Y to S
#     fold(∪, Map(f), S) or Map(any ∘ from_tag(S)) ∘ curry(swap(uncurry(f)))
# \neg S_1 and \neg S_1 \cap S_2 in X
#     !S_1 and S_1 ∩ S_2
# Glue together S_1 \to Z and S_2 \to Z into S_1 \cup S_2 \to Z
#     colimit(s1::Subset, s2::Subset)(f_1::FiniteFunction, f_2::FiniteFunction)
# Make intersection: monomorphisms f_1 : X \to Y, f_2 : X \to Y go to
#     a equalizer S \to X where f_1 and f_2 are equal on
#     from_tag(f1 == f2)
# Go from partition to f : S \to n to subsets $f^{-1}(i)$.
#     i.e. a preimage goes from n to subsets.
#     Normal preimage should return subsets!
#     preimage(f)(i)
# Broadcasting on f.(A) may create duplicates and then supports reduce.
#     Works with iteration
# Supports map(f, A)
#     Does not work unless you use Map(f) from Transducers
# Supports reduce(op, A)
#     Works with transudcers
#     fold(+, A)
# Intersect partitions S \to m and S \to n to S \to m \times n
#     zip((f, g))

struct Subset{S, T}
    subset::S
    universe::T
end

function Base.iterate(s::Subset)
    iterate(s.subset)
end

function Base.iterate(s::Subset, i)
    iterate(s.subset, i)
end

function Base.first(s::Subset)
    first(s.subset)
end

function Base.only(s::Subset)
    only(s.subset)
end

function Base.collect(s::Subset)
    collect(s.subset)
end

function tag(s::Subset)
    FiniteFunction(
        x -> x ∈ s.subset,
        s.universe,
        [false, true]
    )
end

function mono(s::Subset)
    FiniteFunction(
        identity,
        s.subset,
        s.universe
    )
end

function set(s::Subset)
    return s.subset
end

function preimage(s::Subset)
    FiniteFunction(
        x -> if x
            s
        else
            !s
        end,
        [false, true],
        Subset
    )
end

# Is this literally the only operation not supported by Set?
function Base.:!(s::Subset)
    Subset(
        setdiff(s.universe, s.subset),
        s.universe
    )
end

function Base.:∩(s1::Subset, s2::Subset)
    @assert s1.universe == s2.universe
    Subset(
        s1.subset ∩ s2.subset,
        s1.universe
    )
end

function Base.:∪(s1::Subset, s2::Subset)
    @assert s1.universe == s2.universe
    Subset(
        s1.subset ∪ s2.subset,
        s1.universe
    )
end

function from_tag(ff::FiniteFunction)
    # Should this be sorted
    @assert ff.tgt == [false, true]
    Subset(
        Set((x for x in ff.src if ff(x))),
        ff.src
    )
end

function preimage(f::FiniteFunction, ::Type{S}) where {S}
    d = Dict([y => Set{S}() for y in f.tgt])
    for x in f.src
        v = f.f(x)
        if !haskey(d, v)
            error("Unknown target $v evaluated in function")
        end
        push!(d[v], x)
    end
    return (x -> Subset(x, f.src)) ∘ FiniteFunction(
        FunctionDict(d), f.tgt, Subset
    )
end

function get_element_type(T::Type{<:AbstractVector})
    return T.parameters[1]
end

function get_element_type(T::Type{<:AbstractSet})
    return T.parameters[1]
end

function preimage(f::FiniteFunction{F, S}) where {F, S}
    preimage(f, get_element_type(S))
end

function Base.reduce(op, f::FiniteFunction; kwargs...)
    reduce(op, f.(f.src); kwargs...)
end

"""
    reduce_cond(op, f, phi)

This does a reduction operation, except that we get a mapping from
the target of phi to the target of f

``f : X \\to Y,
\\phi : X \\to Z``

Creates mapping from Z to Y. Note that Z must be _finite_ to
get an actually FiniteFunction out. If necessary, you may
need to `reset_target`.
"""
function reduce_cond(op, f::FiniteFunction, phi::FiniteFunction; kwargs...)
    pre_phi = preimage(phi)
    # Should probably use broadcast?
    # Broadcast converts iterators into arrays
    # Maybe turn into for loop?
    # TODO: TRANSDUCERS. This can be parallel within group with
    # foldxd or something
    # And if preimage is already computed then
    (atom -> reduce(op, (f(i) for i in atom); kwargs...)) ∘ pre_phi
end
