"""Finite Function interface"""
module FiniteFunctions

import Tables
using ProgressMeter
import Serialization
using Transducers

include("base.jl")
include("subset.jl")
include("io.jl")

# TODO:
# more testing
# more transducers
# Colimit: A -> B, and A -> C, then we have a A -> B \cup C...
# Fix coproduct and merge
# TimeSeries specific functions: windowing, etc.

#export FiniteFunction
#export preimage, inverse, reduce_cond, rarify, image, invertible, injective
#export reset_src, table_to_ff, checkpoint, curry, uncurry, threadrarify

end
