# FiniteFunctions.jl

Tries to reframe relational data in terms of functions over finite domain.

## Use-Case

1. You are tired of trying to join dataframes in weird ways,
and really all of your dataframes are more like dictionaries
or functions anyhow.

2. You hate that group-by is a completely ad-hoc procedure in SQL and
_please_ just give me a preimage over an appropriate aggregating function.

3. You have been cursed by category theory to wander the earth
in search of categorical constructions of everything.

4. You really like the idea of combining databases / dataframes with graphs,
and having things like structs of arrays instead of arrays of structs.

## Desired Operations

1. $f = g \circ h$.

2. $g(x) = f(x, y)$ (this could just be a combination of $i(x) = (x, y)$ which can be seen as combing a morphism $j(1) = y$ and then using the isomorphim $x \leftrightarrow (x, 1)$.

3. $g(y) = f^{-1}(y)$

4. $g(x) = f(x, -)$ (function valued)

5. $g(x) = \sum_y f(x, y) = \mathbb{E}(f(x, y) | ((x, y) \mapsto x))$.

6. $f(x, y) = g(x) \times g(y)$

7. $dup(x) = (x, x)$.

8. $g(a) = \mathbb{E}(f(b) | \phi)$ where $\phi(b) = a$. So what we really care about is this sort of labelled $\sigma$-algebra $\phi^{-1}$.

9. $h(i, k) = \mathbb{E}\_j(\mathrm{prod} \circ (f \times g) \circ \mathrm{match}\_{j=k}^{-1}(i, j, k, true))$
(matrix multiplication of $f(i, j)$ and $g(k, l)$ ) and $\mathrm{match}((i, j), (k, l)) = (i, j, l, j==k)$

10. $2^X \Leftrightarrow S \hookrightarrow X$.

## Similarities to Catlab.jl

Catlab.jl seeks to provide a similar Categorical interface to relational data through ACSets.
Mathematically, I have no problem with such an endeavour. However practically, most of my time
is spent dealing with either matching tables against each other or dealing with
annoyingly nested simulations, so I have created my own set of tools to deal with this.

https://github.com/AlgebraicJulia/Catlab.jl/blob/1e4da78aa7e1ee0a715755390bc0e97055dda398/src/acsets/Mappings.jl

I'm not exactly looking to reinvent Catlab.jl, but I _am_ looking to reinvent their concrete data structures
for ACSet and finite function access, since their existing ones are difficult to use. For
instance, it would be wonderful to use the homomorphism code that already exists in Catlab.jl,
which may be of immediate practical use in matching power systems.

## Relation to TimeSeries.jl

[TimeSeries.jl](https://github.com/JuliaStats/TimeSeries.jl) seeks to provide similar first class
methods on functions, however they focus on date time objects. Since TimeSeries.jl is
not _that_ maintained and is very focused on time series alone, it would be nice
to just deal with time series as a special case.

## Relation to DataFrames.jl

It would be very nice to be able to turn tables (relations) into proper functions. This
library exports a `table_to_ff` function, but this is rather ad-hoc.

Somewhat better would be something like serialization / deserialization across Tables.jl interfaces:
- "deserialize" one or more Tables.jl data structures into a collection of finite functions (with an ACSet schema)
- "serialize" into one or more CSVs/Feather/HDF5/whatever along with enough schema information (in the form of an ACSet schema)
to recover the original structure.

## Typing options:

In Julia, there is a constant tension between three things:

1. Too many types causes excessive dispatch and compilation time.
2. Too little types removes options for extensibility, performance, and safety!

Some of these can be mitigated with the appropriate nospecialize, but that's kind of unclear,
because sometimes there is some inner function which you do want to specialize. If
possible it would be nicer to have some divisions on the orthogonal problems:

1. You can arbitrarily compile or not compile for whatever makes sense. If you compile,
then the types can be coerced into something simple. Having 5 different "types" of DataFrames should not be an issue.

(Maybe conversions to an appropriate format can be dead simple?)
2. You can arbitrarly type as much as you want. If you add types, then that
doesn't necessarily cause more compilation.


Idea: convert objects to abstract thing which can be turned into objects
without O(n^2) code-gen. We can just have functions that hold it lazily.
